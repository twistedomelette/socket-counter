import {showInputModal, showMessageModal, showResultsModal} from "./views/modal.mjs";
import {appendRoomElement, updateNumberOfUsersInRoom} from "./views/room.mjs";
import {appendUserElement, changeReadyStatus, removeUserElement, setProgress} from "./views/user.mjs";
// import {texts} from "../../src/data.ts";

const texts = [
	"Text for typing #1",
	"Text for typing #2",
	"Text for typing #3",
	"Text for typing #4",
	"Text for typing #5",
	"Text for typing #6",
	"Text for typing #7",
];
const MAXIMUM_USERS_FOR_ONE_ROOM = 5;
const SECONDS_TIMER_BEFORE_START_GAME = 10;
const SECONDS_FOR_GAME = 60;

const username = sessionStorage.getItem('username');

if (!username) {
	window.location.replace('/login');
}

const submitButton = document.getElementById('add-room-btn');
const quitRoom = document.getElementById('quit-room-btn');
const readyButton = document.getElementById('ready-btn');
const myTimer = document.getElementById('timer')
const gameTimer = document.getElementById('game-timer')
const gameTimerSeconds = document.getElementById('game-timer-seconds')
const gameText = document.getElementById('text-container')
const joinButtons = document.getElementsByClassName('join-btn');
const conUser = document.getElementsByClassName('connected-users');
const valueName = document.getElementById('room-name');
// const socket = io('', { query: { username } });
const socket = io('http://localhost:3002/game', { query: { username } });

let roomName = '';
let s = 0;
let numText = Math.floor(Math.random() * texts.length);
gameText.innerText = '_'+texts[numText];
let GameStatus = false;
let EventStatus = false;
let cursor;
const usersInRoom = [];

const InInput = {
	title: 'Enter Room Name',
	onChange: function(e) {
		roomName = e;
	},
	onSubmit: function () {
		const RoomElement = {
			name: roomName,
			numberOfUsers: 1,
			onJoin: function () {
				console.log("Join");
			}
		}
		let status = true;
		for (let el of joinButtons) {
			if (el.attributes[1].value === roomName) {
				showMessageModal({message: "Invalid Name", onCloseError});
				status = false
			}
		}
		if (status) {
			socket.emit("USER_INFO", {id: socket.id, value: readyButton.innerHTML});
			socket.emit("ROOM_INFORMATION", RoomElement);
		}
	}
}

function onCloseError() {
	console.log('error');
}

function root(newValue) {
	valueName.innerText = `Room ${newValue.name}`;
	let numberUser = 0;
	appendRoomElement({...newValue, onJoin});
	for (let el of joinButtons) {
		if (el.attributes[1].value === newValue.name) el.addEventListener('click', (e) => {
			valueName.innerText = `Room ${newValue.name}`;
			for (let el of conUser) {
				if (el.attributes[1].value === newValue.name) {
					numberUser = el.attributes[2].value;
				}
			}
			socket.emit("ADD_PLAYER", {name: newValue.name, numberOfUsers: +numberUser+1})
		})
	}
	//onJoin();
}

function rootTwo(data) {
	let ready;
	ready = data.value !== "READY";
	//updateNumberOfUsersInRoom({name: id, numberOfUsers: 1})
	if (socket.id === data.id) {
		appendUserElement({ username: data.id, ready: ready, isCurrentUser: true })
		const room = document.getElementById('rooms-page');
		const game = document.getElementById('game-page');
		room.classList.add('display-none');
		game.classList.remove('display-none');
	} else {
		appendUserElement({ username: data.id, ready: ready, isCurrentUser: false })
	}
	if (checker()) timer();
}

function checker() {
	let rSt = document.getElementsByClassName("ready-status");
	let check = false;
	for (const el of rSt) {
		if (el.innerHTML === '🔴')
			break;
		if (el === rSt[rSt.length-1]) {
			check = true;
		}
	}
	return check;
}

function timer() {
	myTimer.innerHTML = SECONDS_TIMER_BEFORE_START_GAME.toString();
	readyButton.classList.add("display-none");
	myTimer.classList.remove("display-none");
	let time = +myTimer.innerHTML;
	let interval = setInterval(() => {
		time--;
		myTimer.innerHTML = time.toString();
	}, 1000)
	setTimeout(() => { clearInterval(interval); startGame()}, SECONDS_TIMER_BEFORE_START_GAME*1000);
}

function gTimer() {
	gameTimerSeconds.innerHTML = SECONDS_FOR_GAME.toString();
	gameTimer.classList.remove("display-none");
	let time = +gameTimerSeconds.innerHTML;
	let interval = setInterval(() => {
		time--;
		gameTimerSeconds.innerHTML = time.toString();
		if (!GameStatus) {
			clearInterval(timerEnd)
			clearInterval(interval)
		}
	}, 1000)
	let timerEnd = setTimeout(() => { clearInterval(interval); EventStatus = false; socket.emit("END_MODULE");}, SECONDS_FOR_GAME*1000);
}

function updateProgress(data) {
	const progressEl = document.querySelector(`.user-progress[data-username='${data.id}']`);
	if (progressEl !== null)
		setProgress({username: data.id, progress: data.progress});
}

function showEndModule() {
	GameStatus = false;
	if (!EventStatus) document.removeEventListener('keydown', listener);
	setTimeout(() => {
		numText = Math.floor(Math.random() * texts.length);
		gameText.innerText = '_'+texts[numText];
	}, 1000)
	gameText.classList.add("display-none");
	gameTimer.classList.add("display-none");
	socket.emit("USER_PROGRESS", {id: socket.id, progress: 0});
	socket.emit("USER_READY", {id: socket.id, value: readyButton.innerHTML})
	let g = document.getElementsByClassName('modal');
	if (g.length === 0)
		showResultsModal({usersSortedArray: ['Kate', 'Ted'], onClose})
}

function onClose() {
	readyButton.classList.remove("display-none");
}

function startGame() {
	GameStatus = true;
	EventStatus = true;
	cursor = undefined;
	myTimer.classList.add("display-none");
	gameText.classList.remove("display-none");
	socket.emit("USER_PROGRESS", {id: socket.id, progress: 0});
	gTimer();
	document.addEventListener('keydown', listener)
}

let listener = function addtext(e) {
	if (cursor === undefined) cursor = 1;
	if (e.key === gameText.innerText[cursor]) {
		let progress = 100/(gameText.innerText.length-1)*cursor;
		socket.emit("USER_PROGRESS", {id: socket.id, progress: progress});
		if (cursor === 1)
			gameText.innerText = gameText.innerText.slice(cursor, cursor+1)+gameText.innerText.slice(0, cursor)+gameText.innerText.slice(cursor+1);
		else
			gameText.innerText = gameText.innerText.slice(0, cursor-1)+gameText.innerText.slice(cursor, cursor+1)+gameText.innerText.slice(cursor-1, cursor)+gameText.innerText.slice(cursor+1);
		cursor++;
	}
	if (cursor === gameText.innerText.length) socket.emit("END_MODULE");
}

socket.on("UPDATE_ROOMS", root);
socket.on("CONNECT_USER", rootTwo);
socket.on("STATUS_USER", sReady);
socket.on("OUT_USER", quit);
socket.on("NEW_PROGRESS", updateProgress);
socket.on("SHOW_END_MODULE", showEndModule);
socket.on("USER_CONTROL", updateUser);

function updateUser(data) {
	updateNumberOfUsersInRoom(data);
}
function sReady(data) {
	let status;
	if (data.value === "READY") {
		status = true;
		readyButton.innerHTML = "NOT READY";
	} else if (data.value === "NOT READY"){
		status = false;
		readyButton.innerHTML = "READY";
	} else status = data.change === "READY";
	const readyElement = document.querySelector(`.ready-status[data-username='${data.id}']`);
	if (readyElement !== null) changeReadyStatus({username: data.id, ready: status});
	if (checker()) timer();
}
function onJoin() {
	socket.emit("USER_INFO", {id: socket.id, value: readyButton.innerHTML});
}
function quit(id) {
	let numberUser = 0;
	const RoomValue = valueName.innerText.slice(5);
	for (let el of conUser) {
		if (el.attributes[1].value === RoomValue) {
			numberUser = el.attributes[2].value;
		}
	}
	socket.emit("ADD_PLAYER", {name: RoomValue, numberOfUsers: numberUser-1})
	removeUserElement(id)
	if (socket.id === id) {
		const room = document.getElementById('rooms-page');
		const game = document.getElementById('game-page');
		game.classList.add('display-none');
		room.classList.remove('display-none');
	}
}

submitButton.addEventListener('click', () => showInputModal(InInput));
quitRoom.addEventListener('click', () => socket.emit("USER_QUIT", socket.id));
readyButton.addEventListener('click', () => socket.emit("USER_READY", {id: socket.id, value: readyButton.innerHTML}));
