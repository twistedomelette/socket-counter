import loginRoutes from './loginRoutes';
import gameRoutes from './gameRoutes';
import { Express } from 'express';
import counter from "./counter";

export default (app: Express) => {
	app.use('/login', loginRoutes);
	app.use('/game', gameRoutes);
	app.use('/counter', counter);
};
