import { Server } from 'socket.io';
import * as config from './config';
import user from "./user";
import counter from "./counter";

export default (io: Server) => {
	user(io.of("/game"));
	counter(io.of("/counter"))
	// io.of("/game").on('connection', socket => {
	// 	const username = socket.handshake.query.username;
	// });
};
