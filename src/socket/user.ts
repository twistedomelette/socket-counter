const RoomElement = {
    name: 'Bob',
    numberOfUsers: 0,
}

export default io => {
    io.on('connection', socket => {
        const username = socket.handshake.query.username;
        socket.on("ROOM_INFORMATION", (RoomElement) => {
            socket.emit("UPDATE_ROOMS", RoomElement);
            socket.broadcast.emit("UPDATE_ROOMS", RoomElement);
        })
        socket.on("USER_INFO", (data) => {
            socket.emit("CONNECT_USER", data);
            socket.broadcast.emit("CONNECT_USER", data);
        })
        socket.on("ADD_PLAYER", (data) => {
            socket.emit("USER_CONTROL", data);
            socket.broadcast.emit("USER_CONTROL", data);
        })
        socket.on("USER_PROGRESS", (data) => {
            socket.emit("NEW_PROGRESS", data);
            socket.broadcast.emit("NEW_PROGRESS", data);
        })
        socket.on("USER_READY", (data) => {
            socket.emit("STATUS_USER", data);
            socket.broadcast.emit("STATUS_USER", {id: data.id, change: data.value, s: data.s});
        })
        socket.on("USER_QUIT", (id) => {
            socket.emit("OUT_USER", id);
            socket.broadcast.emit("OUT_USER", id);
        })
        socket.on("END_MODULE", () => {
            socket.emit("SHOW_END_MODULE");
            socket.broadcast.emit("SHOW_END_MODULE");
        })
        socket.on("DELETE_EVENT", () => {
            socket.emit("STOP_EVENT");
            socket.broadcast.emit("STOP_EVENT");
        })
    });
}